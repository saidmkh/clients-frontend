import React, { Component } from 'react';
import './styles/App.css';
import './styles/media.css';

import Main from './components/main/main'

class App extends Component {
  render() {
    return (
      <div className="App">
        <Main />
      </div>
    );
  }
}

export default App;
