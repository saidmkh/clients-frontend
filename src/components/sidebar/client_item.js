import React from 'react'

import { Item } from 'semantic-ui-react'
import { dispatchSelectClient } from '../store/dispatchers'

export default function ClientItem (props) {
  return (
    <Item className="client--item" key={props.idx} onClick={dispatchSelectClient.bind(null, props.obj.contact.email)}>
      <Item.Image size='tiny' src={props.obj.general.avatar} />
      <Item.Content>
        <Item.Header>{props.obj.general.firstName} {props.obj.general.lastName}</Item.Header>
        <Item.Description>{props.obj.job.title}</Item.Description>
      </Item.Content>
    </Item>
    )
}