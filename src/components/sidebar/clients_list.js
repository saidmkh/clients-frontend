import React, { Component } from 'react'
import {connect} from 'react-redux'

import { List } from 'semantic-ui-react'
import store from '../store/store'
import ClientItem from './client_item'

class ClientsList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      clients: []
    }
  }

  componentDidMount() {
   let data = store.getState();

   this.setState({
     clients: data.clients,
   })
 }

 componentWillReceiveProps(nextProps){
   this.setState({clients: nextProps.foundClients})
 }
  render() {
    if (this.state.clients) {
      return (
        <div className="clients-list">
          <List divided size='huge' verticalAlign='top'>
            {this.state.clients.map(function(obj, idx) { 
              return (
                <ClientItem
                  key={idx}
                  idx={idx}
                  obj={obj}
                />
              )
            })}
          </List>
        </div>
      )
    } else {
          return (
            <div className="clients-list" />
          )
        }
  }
}

function mapStateToProps(state, ownProps) {
  return {
      foundClients: state.foundClients
  }
}

export default connect(mapStateToProps)(ClientsList);