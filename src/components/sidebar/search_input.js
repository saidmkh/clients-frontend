import React, { Component } from 'react'
import { Input } from 'semantic-ui-react'
import { search } from '../main/functions'

class SearchInput extends Component {
  render() {
    return (
      <div className="input_block">
        <Input fluid icon='search' placeholder='Search...' onChange={(search.bind(this))}/>
      </div>
    )
  }
}

export default SearchInput;