import React, { Component } from 'react'
import SearchInput from './search_input'
import ClientsList from './clients_list'

class SideBar extends Component {
  render() {
    return (
      <div className="sidebar">
        <SearchInput/>
        <ClientsList />
      </div>
    )
  }
}

export default SideBar;