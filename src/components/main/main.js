import React, { Component } from 'react'

import SideBar from '../sidebar/sidebar'
import Content from '../content/content'

class Main extends Component {
  render() {
    return (
      <div className="wrapper">
        <SideBar />
        <Content />
      </div>
    )
  }
}

export default Main;