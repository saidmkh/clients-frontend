import {dispatchSearchClient} from '../store/dispatchers'
import store from '../store/store'

export function search(e) {
  let value = e.target.value.toLowerCase();
  let clients = store.getState().clients;
  clients = clients.filter(function(item) {
    if (value === '') {
      return true
    } else {
        let firstName_value = item.general.firstName.toLowerCase().indexOf(value) !== -1;
        let secondName_value = item.general.lastName.toLowerCase().indexOf(value) !== -1;
        let company_value = item.job.company.toLowerCase().indexOf(value) !== -1;
        let title_value = item.job.title.toLowerCase().indexOf(value) !== -1;
        let email_value = item.contact.email.toLowerCase().indexOf(value) !== -1;
        let phone_value = item.contact.phone.toLowerCase().indexOf(value) !== -1;
        let street_value = item.address.street.toLowerCase().indexOf(value) !== -1;
        let city_value = item.address.city.toLowerCase().indexOf(value) !== -1;
        let zipCode_value = item.address.zipCode.toLowerCase().indexOf(value) !== -1;
        let country_value = item.address.country.toLowerCase().indexOf(value) !== -1;

        return firstName_value || secondName_value || company_value || title_value || email_value 
        || phone_value || street_value || city_value || zipCode_value || country_value;
       }
      
  })
  dispatchSearchClient(clients);
}

export function mainSideToggle() {
  const content = document.getElementsByClassName('main')[0];const toggleBtn = document.getElementsByClassName('burger-menu')[0];

  content.classList.toggle('toggle-content');
}