import React, { Component } from 'react'
import { Item, Divider } from 'semantic-ui-react'
import {connect} from 'react-redux'


class ClientDetail extends Component {
  render() {
    if (this.props.selectedUser) {
      return (
        <Item.Group>
          <Item>
            <div className="main-photo__block">
              <img src={this.props.selectedUser.general.avatar} alt="" className="main-profile--img" />
            </div>
            <Item.Content>

                <Item.Header>{this.props.selectedUser.general.firstName} {this.props.selectedUser.general.lastName}</Item.Header>
                <Item.Meta><span className="value-title">{this.props.selectedUser.job.title}</span></Item.Meta>
                <Item.Meta>{this.props.selectedUser.job.company}</Item.Meta>
                <Divider fitted />
                <Item.Extra>Other information</Item.Extra>
                <Divider hidden />
                <Item.Meta>Contacts</Item.Meta>
                <Item.Description><span className="value-title">Email: </span>{this.props.selectedUser.contact.email}</Item.Description>
                <Item.Description><span className="value-title">Phone: </span>{this.props.selectedUser.contact.phone}</Item.Description>
                <Divider hidden />
                <Item.Meta>Address</Item.Meta>
                <Item.Description><span className="value-title">Street: </span>{this.props.selectedUser.address.street}</Item.Description>
                <Item.Description><span className="value-title">City: </span>{this.props.selectedUser.address.city}</Item.Description>
                <Item.Description><span className="value-title">Zip code: </span>{this.props.selectedUser.address.zipCode}</Item.Description>
                <Item.Description><span className="value-title">Country: </span>{this.props.selectedUser.address.country}</Item.Description>
              
            </Item.Content>
          </Item>
        </Item.Group>
      )} 
      else {
        return (
          <div className="detail-placeholder">
            Select a client to check info
          </div>
        )
    }
  }
}

function mapStateToProps(state, ownProps) {
  return {
      selectedUser: state.selectedClient,
  }
}

export default connect(mapStateToProps)(ClientDetail);