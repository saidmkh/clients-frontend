import React, { Component } from 'react'
import ClientDetail from '../content/client_detail'
import {mainSideToggle} from '../main/functions'

class Content extends Component {
  render() {
    return (
      <div className="main">
        <div className="burger-menu" onClick={mainSideToggle}>
            <div className="burger-menu-container">
                <div className="burger-menu--line"></div>
                <div className="burger-menu--line"></div>
                <div className="burger-menu--line"></div>
            </div>
        </div>
        <ClientDetail />
      </div>
    )
  }
}

export default Content;