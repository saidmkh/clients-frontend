import store from './store';

export function dispatchSelectClient(client) {
  store.dispatch({
      type: 'CLIENT-SELECTED',
      payload: client
  })
}

export function dispatchSearchClient(item) {
  store.dispatch({
      type: 'SEARCH-CLIENT',
      payload: item
  })
}