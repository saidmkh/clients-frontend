import { createStore } from 'redux'
import clients from '../../fixtures/clients.json'


const initial_state = {
  clients: clients,
  selectedClient: null,
  foundClients: []
}

function reducer (state, action) {
  console.log(state)
  switch (action.type) {
    case 'CLIENT-SELECTED':
      let client_email = action.payload;
      return {...state, ...{
        selectedClient: state.clients.filter(function(obj){ return obj.contact.email === client_email})[0]
    }}
    case 'SEARCH-CLIENT':
      return {...state, ...{
        foundClients: action.payload
    }}
    default:
      return state
  }
}

export default createStore(reducer, initial_state);