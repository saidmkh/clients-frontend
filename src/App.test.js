import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import Main from './components/main/main'
import SideBar from './components/sidebar/sidebar'
import SearchInput from './components/sidebar/search_input'
import ClientsList from './components/sidebar/clients_list'
import ClientItem from './components/sidebar/client_item'
import Content from './components/content/content'
import ClientDetail from './components/content/client_detail'


it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Main />, div);
  ReactDOM.unmountComponentAtNode(div);
})

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<SideBar />, div);
  ReactDOM.unmountComponentAtNode(div);
})

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<SearchInput />, div);
  ReactDOM.unmountComponentAtNode(div);
})

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<ClientsList />, div);
  ReactDOM.unmountComponentAtNode(div);
})

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<ClientItem />, div);
  ReactDOM.unmountComponentAtNode(div);
})

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Content />, div);
  ReactDOM.unmountComponentAtNode(div);
})

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<ClientDetail />, div);
  ReactDOM.unmountComponentAtNode(div);
})
